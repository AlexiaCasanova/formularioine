﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using MySql.Data.MySqlClient;

namespace Formulario_Registros
{
    public partial class Form1 : Form
    {

   
        MySqlConnection Conexion = new MySqlConnection("Server=localhost; Database=optimizada; Uid=root; Pwd=12345678;");
        DataSet ds;
        public Form1()
        {
            InitializeComponent();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Conexion.Open();
            MySqlCommand mostrar = new MySqlCommand("SELECT x.id,c.clave, x.nacio, x.edad, IF(x.g=1,'Hombre','Mujer') as genero ,x.l as Localidad, n.acronimo as Nombre, x.fecha as Fecha,d.domicilio,di.distrito, co.colonia, x.cp as CodigoPostal, oc.ocupacion, x.numero, 'Hermosillo' as ciudad  FROM ciudadanos AS x INNER JOIN claves AS c ON c.Id = x.id INNER JOIN acronimos AS n ON n.id = x.id INNER JOIN domicilios AS d ON d.Id = x.id INNER JOIN distritos AS di ON di.Id = x.id INNER JOIN colonias AS co ON co.id = x.id INNER JOIN ocupaciones AS oc ON oc.id = x.id limit 30", Conexion);

            MySqlDataAdapter m_datos = new MySqlDataAdapter(mostrar);
            ds = new DataSet();
            m_datos.Fill(ds);

            dataGridView1.DataSource = ds.Tables[0];
            Conexion.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Conexion.Open();
            MySqlCommand mostrar = new MySqlCommand("SELECT x.id,c.clave, x.nacio, x.edad, IF(x.g=1,'Hombre','Mujer') as genero ,x.l as Localidad, n.acronimo as Nombre, x.fecha as Fecha,d.domicilio,di.distrito, co.colonia, x.cp as CodigoPostal, oc.ocupacion, x.numero, 'Hermosillo' as ciudad  FROM ciudadanos AS x INNER JOIN claves AS c ON c.Id = x.id INNER JOIN acronimos AS n ON n.id = x.id INNER JOIN domicilios AS d ON d.Id = x.id INNER JOIN distritos AS di ON di.Id = x.id INNER JOIN colonias AS co ON co.id = x.id INNER JOIN ocupaciones AS oc ON oc.id = x.id limit 30 like '%"+ txtBuscar.Text + "%'", Conexion);

            MySqlDataAdapter m_datos = new MySqlDataAdapter(mostrar);
            ds = new DataSet();
            m_datos.Fill(ds);

            dataGridView1.DataSource = ds.Tables[0];
            Conexion.Close();
        }
    }
}
